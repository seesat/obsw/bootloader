/*
 * StateMachine.c
 *
 *  Modified on: 02.06.2024
 *      Project: bootloader-ti-tiarm-with-flash-api
 *       Author: Texas Instruments and Pascal Fiedler
 *     Function: Copying the Flash API to RAM, initialize
 *               everything for UART usage and calling the
 *               bootloader state machine.
 */

#include "config.h"
#include "sci_common.h"
#include "UART.h"
#include "HL_het.h"
#include "HL_gio.h"
#include "HL_sci.h"
#include "HL_system.h"
#include "HL_sys_common.h"
#include "HL_sys_core.h"
#include "StateMachine.h"



/*****************************************************************************
*
* This holds the current remaining size in bytes to be downloaded.
*
******************************************************************************/
uint32_t g_ulTransferSize;

/*****************************************************************************
*
* This holds the current address that is being written to during a download
* command.
*
******************************************************************************/
uint32_t g_ulTransferAddress;

/*****************************************************************************
*
* This is the data buffer used during transfers to the boot loader.
*
******************************************************************************/
uint32_t g_pulDataBuffer[BUFFER_SIZE];

/*****************************************************************************
*
* This is the data buffer used for update status.
*
******************************************************************************/

uint32_t g_ulUpdateBufferSize = 32;    /*16 bytes or 4 32-bit words*/

#define E_PASS     		0
#define E_FAIL     		0x1U
#define E_TIMEOUT  		0x2U

/*****************************************************************************
*
* This is an specially aligned buffer pointer to g_pulDataBuffer to make
* copying to the buffer simpler.  It must be offset to end on an address that
* ends with 3.
*
******************************************************************************/
uint8_t *g_pucDataBuffer;


/*****************************************************************************
*
* Symbols of addresses needed for the copying of the Flash API and const
* section to RAM
*
******************************************************************************/
extern unsigned int apiLoadStart;
extern unsigned int apiLoadSize;
extern unsigned int apiRunStart;

extern unsigned int constLoadStart;
extern unsigned int constLoadSize;
extern unsigned int constRunStart;


/** \fn void delay(unsigned int delayval)
 *  \brief Function for creating a time delay.
 *  \param delayval Number of ticks the system should be delayed
 *
 */
void delay(unsigned int delayval) {
	  while(delayval--);
}


/** \fn main(void)
 *  \brief main-function of the bootloader
 */
void main(void){

    /* Initialize SCI Routines to receive Command and transmit data */
	sciInit();

    /* Copy the flash APIs to SRAM*/
    memcpy(&apiRunStart, &apiLoadStart, (uint32)&apiLoadSize);

    /* Copy the .const section */
	memcpy(&constRunStart, &constLoadStart, (uint32)&constLoadSize);

	delay(50000);

    UART_putString(UART, "Hercules MCU UART BootLoader \r");

    BootloaderStateMachine();

}

