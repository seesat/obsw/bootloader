/*
 * StateMachine.c
 *
 *  Created on: 02.06.2024
 *     Project: bootloader-ti-tiarm-with-flash-api
 *      Author: Pascal Fiedler
 *    Function: Providing the States and Transition
 *              for the bootloader statemachine
 */

#include "StateMachine.h"
#include "config.h"
#include "sci_common.h"
#include "UART.h"
#include "CheckImagesState.h"
#include "RepairUpdateImagesState.h"

#include "HL_gio.h"

/*!
 * \brief Ticks to wait between flashing the LED in Fallback state
 */
#define DELAY_VALUE 10000000

/*!
 *  \brief Function to handle the bootlaoder state machine
 */
void BootloaderStateMachine(){

    BootloaderState currentState = CheckImagesState;
    RepairUpdateInfoStruct imageInfoStruct;

    while(1){
        switch(currentState){

        case CheckImagesState:
            CheckImages(&imageInfoStruct);
            currentState = RepairUpdateImagesState;
            break;

        case RepairUpdateImagesState:
            RepairUpdateImages(&imageInfoStruct, &currentState);
            break;

        case StartApplicationState:
            UART_putString(UART, "\r Jump to application...  ");
            uint32_t transferAddress = imageInfoStruct.startAddressApplicationSlot;
            ((void (*)(void))transferAddress)();
            break;

        case FaultFallbackState:

            gioInit();

            gioPORTB->DIR = (0x01 << 6);  //configure GIOB[6] as output

            for (;;) {
                gioToggleBit(gioPORTB, 6);
                for (int i = 0; i < DELAY_VALUE; i++);
            }

            break;

        }
    }
}


