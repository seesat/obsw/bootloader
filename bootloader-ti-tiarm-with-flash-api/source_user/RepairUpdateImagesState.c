/*
 * RepairUpdateImagesState.c
 *
 *  Created on: 04.06.2024
 *     Project: bootloader-ti-tiarm-with-flash-api
 *      Author: Pascal Fiedler
 *    Function: Providing functionality of the Repair
 *              and Update Images state
 */

#include "RepairUpdateImagesState.h"
#include "UART.h"
#include "Flash.h"

/*!
 * \brief Function which is called in the bootloader state machine and checks if a repair or update is necessary
 * \param imageinfostruct Pointer to the RepairUpdateInfoStruct which will be used in the state machine to store the current states of the various images
 * \param currentstate Pointer to the variable which holds the current state of the bootloader state machine
 */
void RepairUpdateImages(RepairUpdateInfoStruct* imageinfostruct, BootloaderState* currentstate){

    if(CheckForUpdates(imageinfostruct)){
        UpdateImages(imageinfostruct);
    }else if(imageinfostruct->statusApplicationImages != 0 || imageinfostruct->statusBootloaderImages != 0){
        RepairImages(imageinfostruct);
    }

    *currentstate = StartApplicationState;
}

/*!
 *  \brief Function which updates the application/bootloader images via UART connection
 *  \param imageinfostruct Pointer to the RepairUpdateInfoStruct which will be used in the state machine to store the current states of the various images
 */
void UpdateImages(RepairUpdateInfoStruct* imageinfostruct){
    if(imageinfostruct->statusBootloaderImages & UpdateAvailableBootloaderBit){

        //Get the first new image via UART connection and store it in the first image location
        uint32_t* sourceAddress = 0;
        uint32_t* destinationAddress = imageinfostruct->startAddressBootloaderImage1;
        uint32_t imageSize = UpdaterUART((unsigned int)*destinationAddress);

        //memcpy(destinationAddress, sourceAddress, imageSize); //TODO Exchange call with Flash-API

        //Copy the first image location which holds the new version into the other two locations
        sourceAddress = imageinfostruct->startAddressBootloaderImage1;
        destinationAddress = imageinfostruct->startAddressBootloaderImage2;

        //memcpy(destinationAddress, sourceAddress, imageSize); //TODO Exchange call with Flash-API

        destinationAddress = imageinfostruct->startAddressBootloaderImage3;


        //memcpy(destinationAddress, sourceAddress, imageSize); //TODO Exchange call with Flash-API
    }

    if(imageinfostruct->statusApplicationImages & UpdateAvailableApplicationBit){

        //Get the first new image via UART connection and store it in the first image location
        uint32_t* sourceAddress = 0;
        uint32_t* destinationAddress = imageinfostruct->startAddressApplicationImage1;
        uint32_t imageSize = UpdaterUART((unsigned int)*destinationAddress);

        //memcpy(destinationAddress, sourceAddress, imageSize); //TODO Exchange call with Flash-API

        //Copy the first image location which holds the new version into the other location
        sourceAddress = imageinfostruct->startAddressApplicationImage1;
        destinationAddress = imageinfostruct->startAddressApplicationImage2;

        //memcpy(destinationAddress, sourceAddress, imageSize); //TODO Exchange call with Flash-API

    }
}

/*!
 * \brief Functions that checks the stored information if a update is available
 * \param imageinfostruct Pointer to the RepairUpdateInfoStruct which will be used in the state machine to store the current states of the various images
 */
bool CheckForUpdates(RepairUpdateInfoStruct* imageinfostruct){
    return imageinfostruct->statusApplicationImages & UpdateAvailableApplicationBit || imageinfostruct->statusBootloaderImages & UpdateAvailableBootloaderBit;
}

/*!
 * \brief Functions that checks if a image need a repair and if yes the repair operation is started
 * \param imageinfostruct Pointer to the RepairUpdateInfoStruct which will be used in the state machine to store the current states of the various images
 */
void RepairImages(RepairUpdateInfoStruct* imageinfostruct){

    //The repair information is retrieved from the images that got executed
    if(imageinfostruct->statusApplicationImages & RepairBootloaderImage1Bit){
        BootloaderRepair(imageinfostruct->executedImageID, 1,imageinfostruct);
    }

    if(imageinfostruct->statusBootloaderImages & RepairBootloaderImage2Bit){
        BootloaderRepair(imageinfostruct->executedImageID, 2,imageinfostruct);
    }

    if(imageinfostruct->statusBootloaderImages & RepairBootloaderImage3Bit){
        BootloaderRepair(imageinfostruct->executedImageID, 3,imageinfostruct);
    }

    //If both application images need a repair the repair data is requested from the ground station
    if(imageinfostruct->statusApplicationImages & ( RepairApplicationImage1Bit | RepairApplicationImage2Bit)){
        imageinfostruct->statusApplicationImages = imageinfostruct->statusApplicationImages | UpdateAvailableApplicationBit;
        UpdateImages(imageinfostruct);
    }else if(imageinfostruct->statusApplicationImages & RepairApplicationImage1Bit){
        ApplicationRepair(2, 1,imageinfostruct);
    }else if(imageinfostruct->statusApplicationImages & RepairApplicationImage2Bit){
        ApplicationRepair(1, 2,imageinfostruct);
    }
}

/*!
 *  \brief Function that copies intact images to a corrupted one, only for bootloader images
 *  \param sourceimageid ImageID which contains the correct image
 *  \param destinationimageid ImageID which needs to be repaired
 *  \param imageinfostruct Pointer to the RepairUpdateInfoStruct which will be used in the state machine to store the current states of the various images
 */
void BootloaderRepair(uint32_t sourceimageid, uint32_t destinationimageid, RepairUpdateInfoStruct* imageinfostruct){

    if(sourceimageid == destinationimageid){
        return 0;
    }

    uint32_t* sourceAddress = 0;
    uint32_t* destinationAddress = 0;
    uint32_t imageSize = imageinfostruct->bootloaderImageSize;

    switch(sourceimageid){
    case 1:
        sourceAddress = imageinfostruct->startAddressBootloaderImage1;
        break;
    case 2:
        sourceAddress = imageinfostruct->startAddressBootloaderImage2;
        break;
    case 3:
        sourceAddress = imageinfostruct->startAddressBootloaderImage3;
        break;
    }

    switch(destinationimageid){
    case 1:
        destinationAddress = imageinfostruct->startAddressBootloaderImage1;
        break;
    case 2:
        destinationAddress = imageinfostruct->startAddressBootloaderImage2;
        break;
    case 3:
        destinationAddress = imageinfostruct->startAddressBootloaderImage3;
        break;
    }

    //memcpy(destinationAddress, sourceAddress, imageSize); //TODO Exchange call with Flash-API
}

/*!
 *  \brief Function that copies intact images to a corrupted one, only for application images
 *  \param sourceimageid ImageID which contains the correct image
 *  \param destinationimageid ImageID which needs to be repaired
 *  \param imageinfostruct Pointer to the RepairUpdateInfoStruct which will be used in the state machine to store the current states of the various images
 */
void ApplicationRepair(uint32_t sourceimageid, uint32_t destinationimageid, RepairUpdateInfoStruct* imageinfostruct){

    if(sourceimageid == destinationimageid){
        return 0;
    }

    uint32_t* sourceAddress = 0;
    uint32_t* destinationAddress = 0;
    uint32_t imageSize = imageinfostruct->applicationImageSize;

    switch(sourceimageid){
    case 1:
        sourceAddress = imageinfostruct->startAddressApplicationImage1;
        break;
    case 2:
        sourceAddress = imageinfostruct->startAddressApplicationImage2;
        break;
    }

    switch(destinationimageid){
    case 1:
        destinationAddress = imageinfostruct->startAddressApplicationImage1;
        break;
    case 2:
        destinationAddress = imageinfostruct->startAddressApplicationImage2;
        break;
    }

    //memcpy(destinationAddress, sourceAddress, imageSize); //TODO Exchange call with Flash-API
}
