/*
 * CheckImagesState.c
 *
 *  Created on: 02.06.2024
 *     Project: bootloader-ti-tiarm-with-flash-api
 *      Author: Pascal Fiedler
 *    Function: Providing functionality of the Check
 *              Images state
 */

#include "CheckImagesState.h"
#include "CRC32.h"
#include "config.h"
#include "Flash.h"

/*!
 *  \brief Function which is called in the bootlaoder state machine
 *  \param imageinfostruct Pointer to the RepairUpdateInfoStruct which will be used in the state machine to store the current states of the various images
 */
void CheckImages(RepairUpdateInfoStruct* imageinfostruct){
    RetrieveImagesInfo(imageinfostruct);
    ApplicationImagesCheck(imageinfostruct);
}

/*!
 *  \brief Function which extract the information of the various images from the memory and stores it in the imageinfostruct
 *  \param imageinfostruct Pointer to the RepairUpdateInfoStruct which will be used in the state machine to store the current states of the various images
 */
void RetrieveImagesInfo(RepairUpdateInfoStruct* imageinfostruct){
    //imageInfo[0] : Contains Repair/Update Information about Bootloader images
    //imageInfo[1] : Contains Repair/Update Information about Application images
    //imageInfo[2] : Contains Start address of Bootlaoder Image 1
    //imageInfo[3] : Contains Start address of Bootlaoder Image 2
    //imageInfo[4] : Contains Start address of Bootlaoder Image 3
    //imageInfo[5] : Contains Start address of Application Image 1
    //imageInfo[6] : Contains Start address of Application Image 2
    //imageInfo[7] : Contains ID of executed Bootloader Image
    //imageInfo[8] : Contains Address of the Application Entry Point
    uint32_t *imageInfo = (uint32_t *)ImagesStatusAddr;

    imageinfostruct->statusBootloaderImages = imageInfo[0];
    imageinfostruct->statusApplicationImages = imageInfo[1];
    imageinfostruct->startAddressBootloaderImage1 = (uint32_t *)imageInfo[2];
    imageinfostruct->startAddressBootloaderImage2 = (uint32_t *)imageInfo[3];
    imageinfostruct->startAddressBootloaderImage3 = (uint32_t *)imageInfo[4];
    imageinfostruct->startAddressApplicationImage1 = (uint32_t *)imageInfo[5];
    imageinfostruct->startAddressApplicationImage2 = (uint32_t *)imageInfo[6];
    imageinfostruct->executedImageID = imageInfo[7];
    imageinfostruct->startAddressApplicationSlot = imageInfo[8];
    imageinfostruct->bootloaderImageSize = *(imageinfostruct->startAddressBootloaderImage1 + 1);
    imageinfostruct->applicationImageSize = *(imageinfostruct->startAddressApplicationImage1 + 1);

}

/*!
 *  \brief Function which performs the CRC32 operation on the application images and compares it with the stored value
 *  \param imageinfostruct Pointer to the RepairUpdateInfoStruct which will be used in the state machine to store the current states of the various images
 */
void ApplicationImagesCheck(RepairUpdateInfoStruct* imageinfostruct){

    uint32_t crcResult = 0;
    crcResult = CRC32Calculation(imageinfostruct->startAddressApplicationImage1 + 1, imageinfostruct->applicationImageSize);
    imageinfostruct->statusApplicationImages = 0;

    if(crcResult != *imageinfostruct->startAddressApplicationImage1){
        imageinfostruct->statusApplicationImages = imageinfostruct->statusApplicationImages | 0x1;
    }

    crcResult = CRC32Calculation(imageinfostruct->startAddressApplicationImage2 + 1, imageinfostruct->applicationImageSize);

    if(crcResult != *imageinfostruct->startAddressApplicationImage2){
        imageinfostruct->statusApplicationImages = imageinfostruct->statusApplicationImages | 0x10;
    }

    // Fapi_UpdateStatusProgram(ImagesStatusAddr, (uint32_t) &imageinfostruct->statusApplicationImages, sizeof(imageinfostruct->statusApplicationImages)); //TODO Replace with memcpy

}

/*!
 *  \brief Function which performs the CRC32 operation on the bootloader images and compares it with the stored value
 *  \param imageinfostruct Pointer to the RepairUpdateInfoStruct which will be used in the state machine to store the current states of the various images
 */
void BootloaderImagesCheck(RepairUpdateInfoStruct* imageinfostruct){

    uint32_t crcResult = 0;
    crcResult = CRC32Calculation(imageinfostruct->startAddressBootloaderImage1 + 1, imageinfostruct->bootloaderImageSize);
    imageinfostruct->statusBootloaderImages = 0;

    if(crcResult != *imageinfostruct->startAddressBootloaderImage1){
        imageinfostruct->statusBootloaderImages = imageinfostruct->statusBootloaderImages | 0x1;
    }

    crcResult = CRC32Calculation(imageinfostruct->startAddressBootloaderImage2 + 1, imageinfostruct->bootloaderImageSize);

    if(crcResult != *imageinfostruct->startAddressBootloaderImage2){
        imageinfostruct->statusBootloaderImages = imageinfostruct->statusBootloaderImages | 0x10;
    }

    crcResult = CRC32Calculation(imageinfostruct->startAddressBootloaderImage3 + 1, imageinfostruct->bootloaderImageSize);

    if(crcResult != *imageinfostruct->startAddressBootloaderImage3){
        imageinfostruct->statusBootloaderImages = imageinfostruct->statusBootloaderImages | 0x100;
    }

    // Fapi_UpdateStatusProgram(ImagesStatusAddr, (uint32_t) &imageinfostruct->statusBootloaderImages, sizeof(imageinfostruct->statusBootloaderImages)); //TODO Replace with memcpy
}
