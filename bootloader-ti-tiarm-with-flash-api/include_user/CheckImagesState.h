/*
 * CheckImagesState.h
 *
 *  Created on: 02.06.2024
 *     Project: bootloader-ti-tiarm-with-flash-api
 *      Author: Pascal Fiedler
 *    Function: Providing functionality of the Check
 *              Images state
 */

#ifndef __CHECKIMAGESSTATE__
#define __CHECKIMAGESSTATE__

#include "HL_sys_common.h"
/*!
 * \brief Structure to access the stored Information on the various images
 */
typedef struct{
    uint32_t statusBootloaderImages;
    uint32_t statusApplicationImages;
    uint32_t executedImageID;
    uint32_t bootloaderImageSize;
    uint32_t applicationImageSize;
    uint32_t* startAddressBootloaderImage1;
    uint32_t* startAddressBootloaderImage2;
    uint32_t* startAddressBootloaderImage3;
    uint32_t* startAddressApplicationImage1;
    uint32_t* startAddressApplicationImage2;
    uint32_t startAddressApplicationSlot;


} RepairUpdateInfoStruct;


void CheckImages(RepairUpdateInfoStruct* imageinfostruct);
void RetrieveImagesInfo(RepairUpdateInfoStruct* imageinfostruct);
void ApplicationImagesCheck(RepairUpdateInfoStruct* imageinfostruct);
void BootloaderImagesCheck(RepairUpdateInfoStruct* imageinfostruct);

#endif /* __CHECKIMAGESSTATE__ */
