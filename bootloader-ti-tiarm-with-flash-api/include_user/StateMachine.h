/*
 * StateMachine.h
 *
 *  Created on: 02.06.2024
 *     Project: bootloader-ti-tiarm-with-flash-api
 *      Author: Pascal Fiedler
 *    Function: Providing the States and Transition
 *              for the bootloader statemachine
 */

#ifndef __STATEMACHINE__
#define __STATEMACHINE__

/*!
 *  \brief Bootloader state machine States
 *
 */
typedef enum{
    CheckImagesState = 0,       /*!< Value for the CheckImage State */
    RepairUpdateImagesState,    /*!< value for the Repair and Update State */
    StartApplicationState,      /*!< value for the Application Start State */
    FaultFallbackState          /*!< value for the Fault Fallback State */
}BootloaderState;

void BootloaderStateMachine();

#endif /* __STATEMACHINE__ */
