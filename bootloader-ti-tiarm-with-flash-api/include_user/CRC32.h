/*
 * CRC32.h
 *
 *  Created on: 02.06.2024
 *     Project: bootloader-ti-tiarm-with-flash-api
 *      Author: Pascal Fiedler
 *    Function: Provide functionality to calculate CRC32
 *              checksum over a provided memory region
 */

#ifndef __CRC32__
#define __CRC32__

unsigned int CRC32Calculation(const void* data, unsigned int len);

#endif /* __CRC32__ */
