/*
 * RepairUpdateImagesState.h
 *
 *  Created on: 04.06.2024
 *     Project: bootloader-ti-tiarm-with-flash-api
 *      Author: Pascal Fiedler
 *    Function: Providing functionality of the Repair
 *              and Update Images state
 */

#ifndef __REPAIRUPDATEIMAGESSTATE__
#define __REPAIRUPDATEIMAGESSTATE__

#include "StateMachine.h"
#include "config.h"
#include "sci_common.h"
#include "CheckImagesState.h"

/*!
 * \brief Defines to easier access the RepariUpdateInfoStruct->statusApplicationImages and RepariUpdateInfoStruct->statusBootloaderImages variables
 *
 * The starting from the LSB the variables contain the information if a repair is need (0 == no and 1 == yes) and the third/fourth bit contains if
 * a update is for the application/bootloader available (0 == no and 1 == yes).
 */
#define UpdateAvailableBootloaderBit 0x1000
#define UpdateAvailableApplicationBit 0x100
#define RepairBootloaderImage1Bit 0x001
#define RepairBootloaderImage2Bit 0x010
#define RepairBootloaderImage3Bit 0x100
#define RepairApplicationImage1Bit 0x01
#define RepairApplicationImage2Bit 0x10

void RepairUpdateImages(RepairUpdateInfoStruct* imageinfostruct, BootloaderState* currentstate);
bool CheckForUpdates(RepairUpdateInfoStruct* imageinfostruct);
void RepairImages(RepairUpdateInfoStruct* imageinfostruct);
void UpdateImages(RepairUpdateInfoStruct* imageinfostruct);
void BootloaderRepair(uint32_t sourceimageid, uint32_t destinationimageid, RepairUpdateInfoStruct* imageinfostruct);
void ApplicationRepair(uint32_t sourceimageid, uint32_t destinationimageid, RepairUpdateInfoStruct* imageinfostruct);

#endif /* __REPAIRUPDATEIMAGESSTATE__ */
